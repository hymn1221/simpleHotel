 /*
酒店列表,客房列表数据处理模型
* 要先开启运行json-server
* 增加引用外部库，ohpm install @ohos/axios
 */

import http from '@ohos.net.http';
import { LogUtils } from '../utils/LogUtils';
import axios, { AxiosError, AxiosResponse } from '@ohos/axios';
import { HotelItem } from '../model/HotelItem';
import { RoomItem } from '../model/RoomItem';
import axiosClient from '../http/AxiosRequest';


/** 酒店列表接口 http://127.0.0.1:3000/hotelList */
let hotelUrl = '/hotelList';
/** 酒店列表接口 http://127.0.0.1:3000/roomList */
let roomUrl = '/roomList';

/**
 * 酒店列表,客房列表数据处理模型，调用接口取数据
 * @author 黄焕尧
 */
export class HotelRoomViewModel {


  // 一个Http请求网络数据方法, 一样会出现："code":2300007,"message":"Couldn't connect to server"
   testGetHttpData() {
    // 创建一个网络请求对象 （每一个httpRequest对应一个HTTP请求任务，不可复用）
    let httpRequest = http.createHttp();
    httpRequest.request('http://127.0.0.1:3000'+hotelUrl, // HTTP请求的URL地址
      {
        method: http.RequestMethod.GET, // 请求类型 默认为http.RequestMethod.GET
        header: {
          'Content-Type': 'application/json' //发起HTTP请求的响应头。当前是JSON格式字，
        },
        // extraData: {},  // 当使用POST请求时此字段用于传递内容
        connectTimeout: 60000, // 连接超时时间 ，默认为60000ms
        readTimeout: 60000, // 读取超时时间，默认为60000ms
      }, (err, data) => {
        if (!err) { //获取网络数据成功
          AlertDialog.show({message:"获取成功"})
          AlertDialog.show({message:JSON.stringify(data.result)})
          console.log('Result:', data.result); //  获取到网络的返回值
          // 将获取到的数据进行json转换
          console.log('ResultJSON:' + JSON.stringify(data.result).substring(0,50));
        } else { // 获取网络数据失败
          AlertDialog.show({message:'获取失败'})
          console.log('error:' + JSON.stringify(err)); // 请求数据失败反馈
          // 当该请求使用完毕时，调用destroy方法主动销毁
          httpRequest.destroy();
        }
      });
  }

  /**
   * 查询返回酒店列表
   * @returns Promise Array<HotelItem>
   */
  async getHotelItems(): Promise<HotelItem[]> {
    // 使用async….await，async声明发放为异步方法，await等待异步操作执行完毕
    let hotelLit: Array<HotelItem> = [];
    // 发送一个get请求
    await axios.get<Array<HotelItem>, AxiosResponse<Array<HotelItem>>, null>(hotelUrl, axiosClient.config)
      .then((resp:AxiosResponse<Array<HotelItem>>) => {   // 获取数据成功
        if (resp.status === 200) { // 获取正确网络数据
          //获取到网络数据
          hotelLit = resp.data;
          LogUtils.info('获取酒店列表数据完成：' + JSON.stringify(resp.data).substring(0,350));
        }else {
          //获取数据失败打印
          AlertDialog.show({title:'错误',message:'获取酒店列表数据失败!'})
          LogUtils.error('获取酒店列表数据失败！error,' +resp.status);
        }
      })
      .catch((erro:AxiosError)=>{ // 网络异常或者接口异常回调
        AlertDialog.show({title:'错误',message:'获取酒店列表数据失败!'})
        LogUtils.error('获取酒店数据失败！error', erro.message );
        LogUtils.debug('--error错误详情', JSON.stringify(erro));
      });

    return hotelLit;
  }


  /**
   * 查询返回客房列表
   * @returns Promise Array<RoomItem>
   */
  async getRoomItems(): Promise<RoomItem[]> {
    // 使用async….await，async声明发放为异步方法，await等待异步操作执行完毕
    let roomLit: Array<RoomItem> = [];
    // 发送一个get请求
    await axios.get<Array<RoomItem>, AxiosResponse<Array<RoomItem>>, null>(roomUrl, axiosClient.config)
      .then((resp:AxiosResponse<Array<RoomItem>>) => {   // 获取数据成功
        if (resp.status === 200) { // 获取正确网络数据
          //获取到网络数据
          roomLit = resp.data;
          LogUtils.info('获取客房列表数据完成：' + JSON.stringify(resp.data).substring(0,350));
          // 是否有房属性，通过奇偶方式先赋值显示
          roomLit.forEach( (item: RoomItem, index: number, array: RoomItem[]) =>{
            if(index%2 === 0) {
              item.unoccupied = true; }
          });
        }else {
          //获取数据失败打印
          AlertDialog.show({title:'错误',message:'获取客房列表数据失败!'})
          LogUtils.error('获取客房列表数据失败！error,' +resp.status);
        }
      })
      .catch((erro:AxiosError)=>{ // 网络异常或者接口异常回调
        AlertDialog.show({title:'错误',message:'获取客房列表数据失败!'})
        LogUtils.error('获取客房数据失败！error' , erro.message);
        LogUtils.debug('--error错误详情', JSON.stringify(erro));
      });

    return roomLit;
  }

}

// 定义导出变量给页面使用
let hotelRoomModel = new HotelRoomViewModel();
export default hotelRoomModel as HotelRoomViewModel;