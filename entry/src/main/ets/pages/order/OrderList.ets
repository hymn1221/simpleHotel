import { ViewStateConstant } from '../../common/constants/ViewStateConstant'
import { OrderApiParams, OrderListItemData } from '../../model/OrderModel'
import { OrderViewModel } from '../../viewmodel/OrderViewModel'
import { StateComponent } from '../../views/StateComponent'

@Component
export struct OrderList {
  private tabKey: string = "all"
  @State viewState: string = ViewStateConstant.VIEW_STATE_LOADING
  @State orderList: OrderListItemData[] = []
  @State isRefreshing: boolean = false
  private viewModel = new OrderViewModel()

  aboutToAppear() {
    this.viewModel.observeState((state) => {
      console.log(state)
      this.viewState = state
    })
    this.loadNet()
  }

  loadNet() {
    const params: OrderApiParams = {};
    if (this.tabKey === 'finish') {
      params.orderStatus = '20'
    } else if (this.tabKey === 'waitpay') {
      params.orderStatus = '10'
    } else if (this.tabKey === 'waituse') {
      params.orderStatus = '1000'
    }
    this.viewModel.getOrderList(params, (result) => {
      this.orderList = result as OrderListItemData[]
    })
  }

  build() {
    Column() {
      StateComponent({
        viewState: this.viewState, retryCallback: () => {
          this.loadNet()
        }
      }) {
        List({ space: 10, initialIndex: 0 }) {
          ForEach(this.orderList, (item: OrderListItemData, index: number) => {
            ListItem() {
              Column() {
                Column() {
                  Flex({ justifyContent: FlexAlign.SpaceBetween }) {
                    Text(item.hotelName)
                    Text(this.viewModel.getOrderStatus(item.orderStatus))
                      .fontColor($r('app.color.color_999'))
                      .fontSize(14)
                  }

                  Flex({ justifyContent: FlexAlign.Start, alignItems: ItemAlign.Center }) {
                    Image($r('app.media.room_detail_01'))
                      .width($r('app.float.size_80'))
                      .height($r('app.float.size_80'))
                      .border({ radius: $r('app.float.size_10') })
                      .margin({ right: 15 })
                      .flexShrink(0)
                    Column({ space: 5 }) {
                      Row() {
                        Text(item.roomName).fontColor($r('app.color.color_666'))
                        Text('￥' + item.actualPaidPrice).fontColor($r('app.color.color_theme_main')).fontSize(10)
                      }.justifyContent(FlexAlign.SpaceBetween).width('100%')

                      Text(item.nights + '晚' + item.rmQty + '间').fontColor($r('app.color.color_999')).fontSize(10)
                      Row() {
                        Text(item.arrDate + '至' + item.depDate).fontColor($r('app.color.color_999')).fontSize(10)
                        Text('x' + item.rmQty).fontSize(14)
                      }.justifyContent(FlexAlign.SpaceBetween).width('100%')
                    }.alignItems(HorizontalAlign.Start)
                    .flexGrow(1)

                  }.margin({ top: 10 }).width('100%')
                }
                .width('95%')
                .backgroundColor($r('app.color.color_white'))
                .border({ radius: $r('app.float.size_5') })
                .padding(10)
              }.width('100%').justifyContent(FlexAlign.Center)
            }
          })
        }
        .listDirection(Axis.Vertical) // 排列方向
        .width('100%')
        .margin({ top: 10, bottom: 10 })
      }
    }.justifyContent(FlexAlign.Start).width('100%').height('100%')
  }
}