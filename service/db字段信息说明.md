

### 1. 酒店信息

```
hotelList
{ //酒店信息
  "id": 1, //酒店id主键
  "name": "舜和大酒店（天禧店）", // 酒店名称
  "totalReviewsNum": "0", //评论数量
  "address": "中国济南经十纬五路133号", //酒店地址
  "price": "322",//价格
  "imageUrl": "https://agc-storage-drcn.platform.dbankcloud.cn/v0/sunhe-kj08r/hotel%2Fshgj_tx_icon.png?token=981eda66-75f8-48de-9ff8-d2a863dd79af", //酒店图片
  "labelList": [
    "特价房",
    "婚宴厅",
    "会议预定"
  ] //酒店标签
},
```

### 2. 客房信息

```
 roomList
 { //客房信息
  "id": 46, //客房id 主键
  "roomName": "高级大床房", //客房名称
  "imageUrl": "https://agc-storage-drcn.platform.dbankcloud.cn/v0/sunhe-kj08r/room%2Froom_icon%2FshunHe_changQing%2Fgj_dc_icon.png?token=a92a4227-2c7e-43f8-84fd-9b83dc4ca054", // 客房主图片
  "price": "322",//价格
  "labelList": [ //客房标签
    "20㎡",
    "5层",
    "1.8米大床",
    "有窗"
  ]
},
```

### 3. 酒店美食

```
delicacyList
{//酒店美食
  "shopInfo": { //商户信息
    "shopId": 9, //商户id
    "shopName": "舜和国际酒店（济南店）", //商户名称
    "image": "/profile/upload/2023/12/19/国际店-展示图_20231219093615A044.png",//图片
    "address": "济南市经十路26008号"//酒店地址
  },
  "packageInfo": { //美食信息
    "shopMonthlySell": 652, // 已售数量
    "packages": [
      {
        "images": "/profile/upload/2023/12/19/国际店-巴西烤肉单人自助餐_20231219093807A057.png",//图片
        "monthlySell": 100, //月售
        "discountedPrice": 132, //折扣价
        "price": 139, //原价
        "packageName": "巴西烤肉单人自助餐",  //套餐名称
        "id": 20 //套餐id 主键
      },
    ]
  }
},
```

### 4. 宴会厅酒店列表

```
banquetHotelList //宴会厅酒店列表
{
  "id": 3, //酒店id 主键
  "hotelName": "舜和国际酒店", // 酒店名称
  "hotelAddress": "济南市经十路26008号（近济南西站）", //酒店 地址
  "displayImg": "/profile/upload/2024/02/07/宴会酒店列表图_20240207170814A029.png", // 酒店展示图
  "maxArea": "1300㎡", // 酒店面积
  "maxCapacity": "1200人" // 可容纳人数
},
```

### 5. 宴会厅列表

```
banquetList //宴会厅列表
{
  "id": 14, //宴会厅id 主键
  "banquetName": "401鲁韵厅", //宴会厅名称
  "labelList": [ // 
    "会议厅"
  ],
  "banner":"/profile/upload/2024/03/06/企业微信截图_17097139656164_20240306163439A015.png"  // 宴会厅图片
},
```

### 6. 订单列表

```
orderList // 订单列表
{
  "id": 1, //订单id 主键
  "orderStatus": "13", //订单状态（线上取消10，线下取消11，退款成功13，已完成20，退款失败15，未支付0）
  "hotelName": "舜和国际酒店天禧店", //酒店名称
  "roomName": "雅悦双床房", //客房名称
  "arrDate": "2023-11-24", //入住时间
  "depDate": "2023-11-25", //离店时间
  "nights": 1, //入住晚数
  "rmQty": 1, //房间数量
  "actualPaidPrice": "123" //实付金额
},
```

### 7. 用户信息

```
"user": { //用户信息
  "id": 1, //用户id 主键
  "name": "lisi", //名字
  "nickName": "lisi", //名称
  "sex": "0", //性别（男0，女1）
  "tel": "18296498526", //手机号
  "birthday": "2001-11-11" //生日
},
```

### 8. 评论-客人说功能

```
comments //评论
{ 
  "id": 1,  // 评论id 主键
  "commentMakerName": "1***2", //评论人名字
  "comment": "很不错，环境卫生，服务很贴心，用餐体验也可以，菜品新鲜", // 评论内容
  "score": 5, //评论分数（5分最高）
  "commentTime": "2024-02-26 11:55:31", //评论时间
  "hotelName": "舜和国际酒店（济南店）" // 评论酒店
},
```

### 9. 中奖信息

```
"winRecord": //中奖信息
  {
    "id": 1, //中奖id 主键 
    "shopName": "43度茅台500ml(价值1099)", //奖品名称
    "image": "https://www.csohoh.com/images/vip.png", //奖品图片
    "expiryDate": "2024.05.20-2024.06.20", //兑换时间
    "status": "1" //状态 （待领取1，已领取2，已过期3）
  },
```